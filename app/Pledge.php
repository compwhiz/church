<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Pledge
 *
 * @property int $id
 * @property int $project_id
 * @property int $amount
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $member_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Pledge whereMemberId($value)
 * @property-read \App\Member $member
 * @property-read \App\Project $project
 */
class Pledge extends Model
{
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id');
    }

    public function member()
    {
        return $this->hasOne(Member::class, 'id', 'member_id');
    }

    public function payments()
    {
        return $this->hasMany(PledgeProject::class,'member_id','id');
    }
}
