<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    protected $projects;

    /**
     * ProjectController constructor.
     * @param $projects
     */
    public function __construct(Project $projects)
    {
        $this->projects = $projects;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->projects::all();
        return view('projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = $this->projects::all();
        return view('projects.create', compact('projects'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'startDate' => ['required', 'date'],
            'projectName' => ['required',],
            'endDate' => ['required', 'date'],
            'budget' => ['required', 'integer'],
        ]);
        $this->projects->startDate = $request->startDate;
        $this->projects->endDate = $request->endDate;
        $this->projects->projectName = $request->projectName;
        $this->projects->budget = $request->budget;
        $this->projects->saveOrFail();

        return $this->projects;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $contributions = $project->pledges()->with(['member','payments'])->get();

//        $contributions = new ProjectResource($project->load('pledges.member'));
//        dd(json_encode($contributions));
        return view('projects.show', compact('project', 'contributions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return "Project Deleted";
    }
}
