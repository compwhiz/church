<?php

namespace App\Http\Controllers;

use App\Pledge;
use Illuminate\Http\Request;

class PledgeController extends Controller
{
    protected $pledge;

    /**
     * PledgeController constructor.
     * @param $pledge
     */
    public function __construct(Pledge $pledge)
    {
        $this->pledge = $pledge;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'memberid' => ['required'],
            'projectid' => ['required'],
            'amount' => ['required'],
        ]);
        $this->pledge->project_id = $request->projectid;
        $this->pledge->member_id = $request->memberid;
        $this->pledge->amount = $request->amount;
        $this->pledge->saveOrFail();

        return json_encode($this->pledge::with('member')->where('member_id',$request->memberid)->first());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pledge $pledge
     * @return \Illuminate\Http\Response
     */
    public function show(Pledge $pledge)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pledge $pledge
     * @return \Illuminate\Http\Response
     */
    public function edit(Pledge $pledge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Pledge $pledge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pledge $pledge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pledge $pledge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pledge $pledge)
    {
        //
    }
}
