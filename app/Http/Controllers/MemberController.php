<?php

namespace App\Http\Controllers;

use App\Member;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    protected $members;

    /**
     * MemberController constructor.
     * @param $members
     */
    public function __construct(Member $members)
    {
        $this->members = $members;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = $this->members::all();
        return view('members.view', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $members = $this->members::all();
        return view('members.add', compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => ['required'],
            'address'      => ['required'],
            'occupation'   => ['required'],
            'mobileNumber' => ['required'],
            'yearofbirth'  => ['required', 'date'],
            //            'email' => ['required', 'unique:members,email'],
        ]);
        if ($request->hasFile('photo')) {
            $path = $request->photo->store('passports');
        } else {
            $path = '';
        }
        $this->members->name = $request->name;
        $this->members->address = $request->address;
        $this->members->occupation = $request->occupation;
        $this->members->mobileNumber = $request->mobileNumber;
        $this->members->yearofbirth = $request->yearofbirth;
        $this->members->groups = $request->groups;
        $this->members->hbc = $request->hbc;
        $this->members->photo = $path;
        $this->members->email = $request->email;
        $this->members->title = $request->title;
        $this->members->saveOrFail();

        return response()->json($this->members);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        $tithe = $member->tithe()->get();
//        dd($tithe);
        $projectContribution = $member->load('projects.project');

//        dd($projectContribution);
        return view('members.show', compact('tithe', 'member', 'projectContribution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Member $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        //
    }
}
