<?php

namespace App\Http\Controllers;

use App\Contributiontype;
use Illuminate\Http\Request;
use function PHPSTORM_META\type;

/**
 * Class ContributiontypeController
 * @package App\Http\Controllers
 */
class ContributiontypeController extends Controller
{
    /**
     * @var Contributiontype
     */
    protected $type;


    /**
     * ContributiontypeController constructor.
     * @param Contributiontype $type
     */
    public function __construct(Contributiontype $type)
    {
        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $all = $this->type::all();
        return view('contributions.create', compact('all'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contributionType' => ['required']
        ]);
        $this->type->type = $request->contributionType;
        $this->type->saveOrFail();

        return $this->type;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contributiontype $contributiontype
     * @return \Illuminate\Http\Response
     */
    public function show(Contributiontype $contributiontype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contributiontype $contributiontype
     * @return \Illuminate\Http\Response
     */
    public function edit(Contributiontype $contributiontype)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Contributiontype $contributiontype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contributiontype $contributiontype)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contributiontype $contributiontype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contributiontype $contributiontype)
    {
        //
    }
}
