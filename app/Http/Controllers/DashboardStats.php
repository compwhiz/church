<?php

namespace App\Http\Controllers;

use App\Group;
use App\Member;
use App\Pledge;
use App\Project;
use Illuminate\Http\Request;

class DashboardStats extends Controller
{
    public function index()
    {
        return [
            'members' => $this->allMembers(),
            'projects' => $this->allProjects(),
            'groups' => $this->allGroups(),
            'contribution' => $this->totalContribution(),
        ];
    }

    protected function allMembers()
    {
        $members = resolve(Member::class);
        return $members::all()->count();
    }

    protected function allProjects()
    {
        $projects = resolve(Project::class);
        return $projects::all()->count();
    }

    protected function allGroups()
    {
        $groups = resolve(Group::class);
        return $groups::all()->count();
    }

    protected function totalContribution()
    {
        $contributions = resolve(Pledge::class);

        return $contributions::sum('amount');
    }
}
