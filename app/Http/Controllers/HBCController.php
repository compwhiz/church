<?php

namespace App\Http\Controllers;

use App\HBC;
use Illuminate\Http\Request;

class HBCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('hbc.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hbc.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, HBC $hbc)
    {
        $this->validate($request, [
            'hbc' => ['required']
        ]);
        $hbc->name = $request->hbc;
        $hbc->save();
        return response()->json($hbc);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\HBC $hBC
     * @return \Illuminate\Http\Response
     */
    public function show(HBC $hBC)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\HBC $hBC
     * @return \Illuminate\Http\Response
     */
    public function edit(HBC $hBC)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\HBC $hBC
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HBC $hBC)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\HBC $hBC
     * @return \Illuminate\Http\Response
     */
    public function destroy(HBC $hBC)
    {
        //
    }
}
