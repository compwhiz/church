<?php

namespace App\Http\Controllers;

use App\Member;
use App\Tithe;
use Illuminate\Http\Request;

class TitheController extends Controller
{
    protected $tithe;

    /**
     * TitheController constructor.
     * @param $tithe
     */
    public function __construct(Tithe $tithe)
    {
        $this->tithe = $tithe;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Member $member)
    {
        $members = $member::all();
        return view('tithe.create', compact('members'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => ['required'],
            'memberName' => ['required'],
            'tithe' => ['required'],
        ]);
        $this->tithe->date = $request->date;
        $this->tithe->member_id = $request->memberName;
        $this->tithe->tithe = $request->tithe;
        $this->tithe->saveOrFail();

        return $this->tithe;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tithe $tithe
     * @return \Illuminate\Http\Response
     */
    public function show(Tithe $tithe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tithe $tithe
     * @return \Illuminate\Http\Response
     */
    public function edit(Tithe $tithe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tithe $tithe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tithe $tithe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tithe $tithe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tithe $tithe)
    {
        //
    }
}
