<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Auth::loginUsingId(1);
Auth::routes();

Route::get('/', function () {
    return redirect()->to('admin');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('resource', 'GroupController@resource');

    Route::resource('projects', 'ProjectController');

    Route::resource('pledges', 'PledgeController');

    Route::resource('members', 'MemberController');

    Route::resource('tithe', 'TitheController');

    Route::get('memberreport', 'ReportController@allmembers')->name('allmembers');
    Route::get('projectreportmember/{project}/{pro}', 'ReportController@projectreport')->name('projectreportmember');

    Route::resource('reports', 'ReportController');

    Route::post('group/addmember', 'GroupController@addMember');

    Route::post('group/deleteMember', 'GroupController@deleteMember');

    Route::resource('group', 'GroupController');

    Route::resource('contribution', 'ContributionController');

    Route::resource('contributiontype', 'ContributiontypeController');

    Route::resource('hbc', 'HBCController');

    Route::resource('projectpledgepayment', 'PledgeProjectController');


    Route::group(['prefix' => 'stats'], function () {

        Route::get('dashboard', 'DashboardStats@index');
    });

    // Doa
});


