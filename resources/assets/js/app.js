/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.Bus = new Vue();
Vue.component('addproject', require('./components/projects/addproject'));
Vue.component('addhbc', require('./components/hbc/create'));
Vue.component('projects', require('./components/projects/index'));
Vue.component('addmember', require('./components/members/addmember'));
Vue.component('viewmembers', require('./components/members/view'));
Vue.component('dashboard', require('./components/dashboard'));
Vue.component('addpledge', require('./components/projects/pledge'));
Vue.component('allpledges', require('./components/projects/allpledges'));
Vue.component('tithe', require('./components/tithe/tithe'));
Vue.component('addgroup', require('./components/groups/add'));
Vue.component('viewgroup', require('./components/groups/view'));
Vue.component('addgroupmember', require('./components/groups/addgroupmember'));
Vue.component('groupmembers', require('./components/groups/members'));
Vue.component('addcontributiontype', require('./components/contribtuion/addcontributiontype'));
Vue.component('contributiontype', require('./components/contribtuion/types'));

const app = new Vue({

    el: '#app',

});
