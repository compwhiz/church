@extends('layouts.bravo')
@section('title')
    Contribution Types
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Add Contribution Type</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <addcontributiontype></addcontributiontype>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Contribution Types</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <contributiontype :group="{{ $all }}"></contributiontype>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>

@stop