@extends('layouts.bravo')
@section('title')
    Add HBC
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Add HBC</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <addhbc></addhbc>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">HBC's</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>

@stop