<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
                            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                            </span></div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="#" class="waves-effect"> <span class="hide-menu"> {{ Auth::user()->name }}
                        <span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
            <li class="nav-small-cap m-t-10">--- Main Menu</li>
            <li>
                <a href="{{ url('admin') }}"
                   class="{{ Request::segment(1) == 'admin' && Request::segment(2) == '' ? 'waves-effect active' : ''}}"><i
                            data-icon="&#xe008;"
                            class="linea-icon linea-basic fa-fw"></i> <span
                            class="hide-menu">Dashboard</span></a>
            </li>
            <li class="">
                <a href="{{ url('/projects') }}"
                   class="{{ Request::segment(2) == 'projects' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-tasks fa-fw"></i>
                    <span class="hide-menu">Projects<span class="fa arrow"></span></span></a>

                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'projects' && Request::segment(3) == 'create'? 'waves-effect active' : ''}}">
                        <a href="{{ route('projects.create') }}">Add Project</a>
                    </li>
                    <li class="{{ Request::segment(2) == 'projects' && Request::segment(3) == ''? 'waves-effect active' : ''}}">
                        <a href="{{ route('projects.index') }}">View Projects</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/members') }}"
                   class="{{ Request::segment(2) == 'members' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-user fa-fw"></i>
                    <span class="hide-menu">Members<span class="fa arrow"></span></span></a>

                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'members' && Request::segment(3) == 'create' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('members.create') }}">Add Member</a></li>
                    <li class="{{ Request::segment(2) == 'members' && Request::segment(3) == '' ? 'waves-effect active' : ''}}">
                        <a href="{{ route('members.index') }}">View Members</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/tithe') }}" class="{{ Request::segment(2) == 'tithe' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-money fa-fw"></i>
                    <span class="hide-menu">Tithe<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'tithe'&& Request::segment(3) == 'create' ? 'active' : ''}}">
                        <a href="{{ route('tithe.create') }}">Add Tithe</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/tithe') }}" class="{{ Request::segment(2) == 'group' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-group fa-fw"></i>
                    <span class="hide-menu">Ministries<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == 'create' ? 'active' : ''}}">
                        <a href="{{ route('group.create') }}">Add Ministry</a>
                    </li>
                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == '' ? 'active' : ''}}">
                        <a href="{{ route('group.index') }}">View Ministry</a>
                    </li>
                </ul>
            </li>
            <li class="">
                <a href="{{ url('/tithe') }}" class="{{ Request::segment(2) == 'hbc' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-group fa-fw"></i>
                    <span class="hide-menu">HBC<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == 'create' ? 'active' : ''}}">
                        <a href="{{ route('hbc.create') }}">Add HBC</a>
                    </li>
                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == '' ? 'active' : ''}}">
                        <a href="{{ route('hbc.create') }}">View HBC</a>
                    </li>
                </ul>
            </li>
{{--            <li class="">--}}
{{--                <a href="{{ route('contribution.create') }}"--}}
{{--                   class="{{ Request::segment(2) == 'contribution' ? 'waves-effect active' : ''}}">--}}
{{--                    <i data-icon="&#xe008;" class="fa fa-gift fa-fw"></i>--}}
{{--                    <span class="hide-menu">Contribution<span class="fa arrow"></span></span></a>--}}
{{--                <ul class="nav nav-second-level">--}}
{{--                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == 'create' ? 'active' : ''}}">--}}
{{--                        <a href="{{ route('contributiontype.create') }}">Add Contribution Type</a>--}}
{{--                    </li>--}}
{{--                    <li class="{{ Request::segment(2) == 'group' && Request::segment(3) == 'create' ? 'active' : ''}}">--}}
{{--                        <a href="{{ route('contributiontype.create') }}">Add Contribution</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
            <li class="">
                <a href="{{ route('reports.index') }}"
                   class="{{ Request::segment(2) == 'reports' ? 'waves-effect active' : ''}}">
                    <i data-icon="&#xe008;" class="fa fa-file-excel-o fa-fw"></i>
                    <span class="hide-menu">Reports<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li class="{{ Request::segment(2) == 'reports' && Request::segment(3) == 'create' ? 'active' : ''}}">
                        <a href="{{ route('reports.create') }}">Member Reports</a>
                    </li>
                </ul>
            </li>


        </ul>
    </div>
</div>