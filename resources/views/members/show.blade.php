@extends('layouts.bravo')
@section('title')
    Showing member
@stop
<?php /** @var \App\Member $member */ ?>

@section('content')
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"><img width="100%" alt="user" src="{{ asset('plugins/images/big/member.jpg') }}">
                </div>
                <div class="user-btm-box">
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Member Name</strong>
                            <p><b class="text-blue">{{ $member->title ?? $member->title}} </b>{{ $member->name }}</p>
                        </div>
                        <div class="col-md-6"><strong>Address</strong>
                            <p>{{ $member->address }}</p>
                            `
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Email</strong>
                            <p>{{ $member->email }}</p>
                        </div>
                        <div class="col-md-6"><strong>Phone</strong>
                            <p>{{ $member->mobileNumber }}</p>
                        </div>
                    </div>
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>HBC</strong>
                            <p>{{ $member->hbc }}</p>
                        </div>
                        <div class="col-md-6"><strong>Group</strong>
                            <p>{{ $member->groups }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-12"><strong>Contributions</strong>

                        </div>
                    </div>
                    <hr>
                    <!-- /.row -->
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <div class="row">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Tithe</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($tithe as $item)
                            <tr>
                                <?php /** @var \App\Tithe $item */ ?>
                                <td>{{ number_format($item->tithe,2) }}</td>
                                <td>{{ $item->date }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="white-box">
                <div class="row">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Project</th>
                            <th>Amount Pledged</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>

                        </tr>
                        @foreach ($projectContribution->projects as $project)
                            <tr>
                                <td>{{ $project->project->projectName }}
                                <td>{{ $project->amount }}
                            </tr>
                        @endforeach
                        {{----}}
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
@stop