@extends('layouts.bravo')
@section('title')
    Add Member
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Add Member</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <addmember :hbcs="{{ \App\HBC::all() }}"></addmember>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Members</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <viewmembers :members="{{ $members }}" ></viewmembers>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>

@stop