@extends('layouts.bravo')
@section('title')
    Group: {{ $group->name }}
@stop
<?php /** @var \App\Project $project */ ?>
@section('content')
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"><img width="100%" alt="user" src="{{ asset('plugins/images/big/group.jpg') }}">
                </div>
                <div class="user-btm-box">

                    <div class="col-md-12">
                        <addgroupmember :lot="{{$group}}" :members="{{\App\Member::all()}}" ></addgroupmember>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <groupmembers :people="{{ $members }}" :group="{{ $group }}"></groupmembers>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop