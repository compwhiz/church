@extends('layouts.bravo')
@section('title')
    Add Group
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">Add Group</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <addgroup></addgroup>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default block3" style="position: static; zoom: 1;">
                <div class="panel-heading">View Group</div>
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <viewgroup :groups="{{$groups}}"></viewgroup>
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>

@stop